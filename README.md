# Taller de GIT

## ¿Qué es un sistema de control de versiones?
Es un sistema que registra los cambios realizados en un archivo o conjunto de archivos a lo largo del tiempo, de modo que puedas recuperar versiones específicas más adelante

## Sistemas de control de versiones locales
![Sistemas de control de versiones locales](https://git-scm.com/figures/18333fig0101-tn.png)
Para hacer frente a este problema, los programadores desarrollaron hace tiempo VCSs locales que contenían una simple base de datos en la que se llevaba registro de todos los cambios realizados sobre los archivos

## Sistemas de control de versiones centralizados
![Sistemas de control de versiones centralizados](https://git-scm.com/figures/18333fig0102-tn.png)
-  El siguiente gran problema que se encuentra la gente es que necesitan colaborar con desarrolladores en otros sistemas.
-  Estos sistemas, como CVS, Subversion, y Perforce, tienen un único servidor que contiene todos los archivos versionados, y varios clientes que descargan los archivos desde ese lugar central
-  Durante muchos años éste ha sido el estándar para el control de versiones

Sin embargo, esta configuración tiene serias desventajas. La más obvia es el punto único de fallo que representa el servidor centralizado. Si ese servidor se cae durante una hora, entonces durante esa hora nadie puede colaborar o guardar cambios versionados de aquello en que están trabajando. Si el disco duro en el que se encuentra la base de datos central se corrompe, y no se han llevado copias de seguridad adecuadamente, pierdes absolutamente todo —toda la historia del proyecto salvo aquellas instantáneas que la gente pueda tener en sus máquinas locales.

## Sistemas de control de versiones distribuidos
![Sistemas de control de versiones distribuidos](https://git-scm.com/figures/18333fig0103-tn.png)
-   Los clientes no sólo descargan la última instantánea de los archivos: replican completamente el repositorio
-   Si el servidor muere, y estos sistemas estaban colaborando a través de él, cualquiera de los repositorios de los clientes puede copiarse en el servidor para restaurarlo

## Comandos más usados

###  git init
-  Nos permite iniciar un repositorio local para un proyecto que NO se encuentra versionado
-  Creará una carpeta oculta llamada .git/

### git pull / git add / git commit / git push / git status
-  **pull** o estirar nos permite actualizar un repositorio que tenemos en forma local
-  **add** acepta los archivos para ser versionados
-  **commit** confirma la aceptación de los archivos
-  **push** o empujar nos permite enviar todos los cambios aceptados al servidor  - git push origin master 
-  **status** nos muestra la situación del los archivos locales

### Estados de los Archivos
![Estado de los archivos](https://git-scm.com/book/en/v2/images/lifecycle.png)

#### Importante!
    push sólo enviará los cambios que hayamos aceptados los demás quedarán en nuestras máquinas

#### Importante!
    Cuando queremos que git ignore archivos como por ejemplo los archivos de los IDEs (nbproject, .idea, etc) 
    creamos un archivo .gitignore en la raíz del proyecto y agregamos los nombres de  archivos o carpetas

### Remotes
Los servidores a donde irán a para nuestros códigos versionados se llaman “remotes”  
-  **git remote -v:** Lista los repositorios remotos  
-  **git remote add  origin  http://devapp.dncp.gov.py:81/jardissone/taller-de-git.git:** Agrega un servidor remoto con el nombre “origin”  
- **git remote rm origin:** borra un servidor remoto

#### Importante! 
    - Por cada commit se tienen que mirar todos los cambios realizados y revisar que sean los que realmente queremos enviar
    - Para no volvernos locos contamos con la herramienta git-gui

### git clone
- **git clone URL_DEL_REPOSITORIO:** Clona un proyecto que se encuentra en el servidor también llamado origin (nombre por defecto pero podemos llamarle como queramos)

### git diff
- Nos muestra la diferencia que existente en nuestros archivos

    RECOMENDACIÓN: usar git gui

### Ramas o Branches
En el día a día del trabajo con Git una de las cosas útiles que podemos hacer es trabajar con ramas. Las ramas son caminos que puede tomar el desarrollo de un software, algo que ocurre naturalmente para resolver problemas o crear nuevas funcionalidades. En la práctica permiten que nuestro proyecto pueda tener diversos estados y que los desarrolladores sean capaces de pasar de uno a otro de una manera ágil.

La rama “master” en Git no es una rama especial. Es como cualquier otra rama. La única razón por la cual aparece en casi todos los repositorios es porque es la que crea por defecto el comando git init y la gente no se molesta en cambiarle el nombre.
- **git branch:** permite ver la lista de ramas
- **git checkout rama-existente:** Nos mueve de una rama a otra
- **git checkout -b nombre-de-la-rama:** crea una rama con el nombre nombre-de-la-rama
- **git stash:** nos permite dejar los cambios pendientes en un lugar temporal para movernos de rama
- **git stash apply:** nos permite volver recuperar nuestros cambios

### Mezclando ramas
- Cuando existen dos o más ramas deberán ser mezcladas para contar con todas las funcionalidades en una rama específica
- Esto se realiza estando parado en la rama en la que queremos que los cambios se actualicen y se ejecuta:
**git merge rama-que-queremos-mezclar**

### Resolución de Conflictos
- Cuando dos o más personas tocan las mismas líneas de código el primer cambio será aceptado por git mientras que a los demás se les generará un conflicto
- La persona que reciba el conflicto tiene que resolverlo antes de volver a enviarlo al servidor
- La resolución de conflictos se realiza comparando ambas versiones del código y decidiendo cual va a quedar

### Milestones
- Los milestones o hitos son objetivos en el tiempo
- Los desarrolladores organizan sus trabajos (branches) haciendo merge request a un hito específico para luego poder organizar la aceptación de los cambios realizados
- Un milestone se convertirá en una TAG que será una “foto” del repositorio y contendrá todos los cambios realizados hasta ese momento

### Tags
- **git tag:** muestra la lista de tags creados
- **git tag -a x.y.z:** Etiqueta o taggea nuestro proyecto en el último commit realizado
- **git tag -d x.y.z:** borra un tag de nuestro repositorio local
- **git push origin :refs/tags/x.y.z:** elimina un tag del repositorio remoto “origin”
- **git push origin --tags:** envía las etiquetas nuevas al remote

### git flow
![git flow](https://3.bp.blogspot.com/-SkCfv54l9GM/WD0MOERx1dI/AAAAAAAAAWo/9csdcgmAnSgbmErqxVAtWjs03Wh8MMFwQCLcB/s1600/gitflow_horizontal.png)

### Referencias

- https://git-scm.com/book/es/v2
- https://help.github.com/categories/managing-remotes/
- http://bitacoratl.blogspot.com/2016/11/instalacion-y-uso-de-git-flow.html
